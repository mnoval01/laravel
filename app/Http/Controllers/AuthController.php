<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.biodata');
    }

    public function submit(Request $request){
        $nama = $request['fname'];
        $lastname = $request['lname'];
        return view('halaman.selamat', compact('nama','lastname'));
    }
}
