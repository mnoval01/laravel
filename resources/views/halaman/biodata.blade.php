<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
    @csrf
    <label for="fname">First name:</label>
    <br><br>
    <input type="text" id="fname" name="fname">
    <br><br>
    <label for="lname">Last name:</label>
    <br><br>
    <input type="text" id="lname" name="lname"> <br> <br>
    
    <label>Gender:</label> <br>
    <input type="radio" id="male" name="gender" value="MALE">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="FEMALE">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="OTHER">
    <label for="other">Other</label> <br> <br>
   
    <label for="nationality">Nationality:</label>
    <br><br>
    <select name="nationality" id="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Arabian">Arabian</option>
    </select> <br> <br>

    <label>Languange Spoken:</label> <br>
    <input type="radio" id="Indonesian" name="lng" value="ID">
    <label for="Indonesian">Indonesian</label><br>
    <input type="radio" id="Malaysian" name="lng" value="MY">
    <label for="Malaysian">Malaysian</label><br>
    <input type="radio" id="Arabic" name="lng" value="AR">
    <label for="Arabic">Arabian</label> <br> <br>

    <label for="Bio">Bio:</label>
    <br>
    <textarea id="Bio" name="Bio" rows="4" cols="50"></textarea>
    <br>
    <button >Sign Up</button>
</form>
